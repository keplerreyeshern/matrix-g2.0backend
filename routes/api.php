<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\BlogController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\ComponentsController;
use App\Http\Controllers\Api\DashboardController;
use App\Http\Controllers\Api\GalleryController;
use App\Http\Controllers\Api\HomeController;
use App\Http\Controllers\Api\ImageController;
use App\Http\Controllers\Api\NewsController;
use App\Http\Controllers\Api\PasswordResetController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/home', [HomeController::class, 'index']);
Route::get('/home/{news}', [HomeController::class, 'show']);
Route::get('/home/plus/{type}', [DashboardController::class, 'plus']);
Route::post('/contact', [homeController::class, 'contact']);

Route::get('/components', [ComponentsController::class, 'index']);

Route::get('/password/find/{token}', [PasswordResetController::class, 'find']);
Route::post('/password/create', [PasswordResetController::class, 'create']);
Route::post('/password/reset', [PasswordResetController::class, 'reset']);



Route::group(['middleware' => ['auth:api']], function(){
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    // Dashboard
    Route::get('/dashboard', [DashboardController::class, 'index']);


    // Users
    Route::resource('/users', UserController::class);
    Route::get('/users/verify/{email}', [UserController::class, 'verify']);

    // Categories
    Route::resource('/categories', CategoryController::class);
    Route::get('/categories/parent/{category}', [CategoryController::class, 'create']);
    Route::post('/categories/update/{category}', [CategoryController::class, 'update']);
    Route::get('/categories/children/{category}', [CategoryController::class, 'children']);

    // Products
    Route::resource('/products', ProductController::class);
    Route::post('/products/update/{product}', [ProductController::class, 'update']);
    Route::get('/products/children/{category}', [ProductController::class, 'children']);

    // Galleries
    Route::resource('/galleries', GalleryController::class);
    Route::post('/galleries/update/{product}', [GalleryController::class, 'update']);

    // News
    Route::resource('/news', NewsController::class);
    Route::post('/news/update/{news}', [NewsController::class, 'update']);

    // Blogs
    Route::resource('/blogs', BlogController::class);
    Route::post('/blogs/update/{blog}', [BlogController::class, 'update']);

    // Images
    Route::resource('/images', ImageController::class);

});


