<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\Image;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str as Str;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = News::where('type', 'blogs')->get();

        return $blogs;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => $request['language']]);
        $blog = new News();
        $blog->title = $request['title'];
        $blog->slug = Str::slug($request['title']);
        $blog->content = $request['content'];
        $blog->intro = $request['intro'];
        $blog->type = 'blogs';
        $blog->date = date_create('now');
        $file = $request->file('image');
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/blogs/' . $name,  \File::get($file));
            $blog->image = '/storage/images/blogs/' . $name;
        }
        $blog->save();
        if ($request['fileslength'] != '0' ){
            $gallery = new Gallery();
            $gallery->name = $request['title'];
            $gallery->slug = Str::slug($request['title']);
            $gallery->type = 'blogs';
            $gallery->reference = $blog->id;
            $gallery->save();
            for($i=0; $i<$request['fileslength']; $i++){
                $image = new Image();
                $file = $request->file('file'.$i);
                $name =  time()."_".$file->getClientOriginalName();
                Storage::disk('public')->put('/images/galleries/blogs/' . $name,  \File::get($file));
                $image->name = '/storage/images/galleries/blogs/' . $name;
                $image->slug = Str::slug($name);
                $image->gallery_id = $gallery->id;
                $image->save();
            }
        }


        return $blog;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = News::findOrFail($id);
        $gallery = Gallery::where('type', 'blogs')->where('reference', $blog->id)->first();
        if($gallery){
            $images = Image::where('gallery_id', $gallery->id)->get();
        } else {
            $images = [];
        }
        $result = [
            'blog' => $blog,
            'gallery' => $gallery,
            'images' => $images,
        ];
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = News::findOrFail($id);
        if($blog->active){
            $blog->active = false;
        } else {
            $blog->active = true;
        }

        $blog->save();
        return $blog;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => $request['language']]);
        $blog = News::findOrFail($id);
        $blog->title = $request['title'];
        $blog->slug = Str::slug($request['title']);
        $blog->content = $request['content'];
        $blog->intro = $request['intro'];
        $file = $request->file('image');
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/blogs/' . $name,  \File::get($file));
            $blog->image = '/storage/images/blogs/' . $name;
        }
        $blog->save();
        if ($request['fileslength'] != '0' ){
            $gallery = new Gallery();
            $gallery->name = $request['title'];
            $gallery->slug = Str::slug($request['title']);
            $gallery->type = 'blogs';
            $gallery->reference = $blog->id;
            $gallery->save();
            for($i=0; $i<$request['fileslength']; $i++){
                $image = new Image();
                $file = $request->file('file'.$i);
                $name =  time()."_".$file->getClientOriginalName();
                Storage::disk('public')->put('/images/galleries/blogs/' . $name,  \File::get($file));
                $image->name = '/storage/images/galleries/blogs/' . $name;
                $image->slug = Str::slug($name);
                $image->gallery_id = $gallery->id;
                $image->save();
            }
        }


        return $blog;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = News::findOrFail($id);
        $blog->delete();

        return $blog;
    }
}
