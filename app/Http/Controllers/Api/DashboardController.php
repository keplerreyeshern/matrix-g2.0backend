<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Accountant;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accountantsT = Accountant::where('type', 'telephone')->get();
        $accountantsW= Accountant::where('type', 'whatsapp')->get();
        $result = [
            'accountantsT' => $accountantsT,
            'accountantsW' => $accountantsW
        ];
        return $result;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function plus($type)
    {
        $accountant = new Accountant();
        $accountant->month = date("m");
        $accountant->type = $type;
        $accountant->save();
        return $accountant;
    }
}
