<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Gallery;
use App\Models\Image;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str as Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::get();

        return $products;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::whereNull('parent_id')->get();

        return $categories;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function children($id)
    {
        $categories = Category::where('parent_id', $id)->get();

        return $categories;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => $request['language']]);
        $product = new Product();
        $product->name = $request['name'];
        $product->slug = Str::slug($request['name']);
        $product->key = $request['key'];
        $product->model = $request['model'];
        $product->description = $request['description'];
        $product->details = $request['details'];
        $file = $request->file('image');
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/products/' . $name,  \File::get($file));
            $product->image = '/storage/images/products/' . $name;
        }
        $product->save();
        $categories = explode(",", $request['categories']);
        $product->categories()->sync($categories);
        if ($request['fileslength'] != '0' ){
            $gallery = new Gallery();
            $gallery->name = $request['name'];
            $gallery->slug = Str::slug($request['name']);
            $gallery->type = 'products';
            $gallery->reference = $product->id;
            $gallery->save();
            for($i=0; $i<$request['fileslength']; $i++){
                $image = new Image();
                $file = $request->file('file'.$i);
                $name =  time()."_".$file->getClientOriginalName();
                Storage::disk('public')->put('/images/galleries/products/' . $name,  \File::get($file));
                $image->name = '/storage/images/galleries/products/' . $name;
                $image->slug = Str::slug($name);
                $image->gallery_id = $gallery->id;
                $image->save();
            }
        }


        return $product;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        $gallery = Gallery::where('reference', $product->id)->where('type', 'products')->first();
        if($gallery){
            $images = Image::where('gallery_id', $gallery->id)->get();
        } else {
            $images = [];
        }

        $result = [
            'product' => $product,
            'images' => $images,
            'gallery' => $gallery,
            'categories' => $product->categories,
        ];
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        if($product->active){
            $product->active = false;
        } else {
            $product->active = true;
        }

        $product->save();
        return $product;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => $request['language']]);
        $product = Product::findOrFail($id);
        $product->name = $request['name'];
        $product->slug = Str::slug($request['name']);
        $product->key = $request['key'];
        $product->model = $request['model'];
        $product->description = $request['description'];
        $product->details = $request['details'];
        $file = $request->file('image');
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/products/' . $name,  \File::get($file));
            $product->image = '/storage/images/products/' . $name;
        }
        $product->save();
        $categories = explode(",", $request['categories']);
        $product->categories()->sync($categories);
        if ($request['fileslength'] != '0' ){
            if ($request['gallery']){
                $gallery = Gallery::findOrFail($request['gallery']);
            } else {
                $gallery = new Gallery();
                $gallery->name = $request['name'];
                $gallery->slug = Str::slug($request['name']);
                $gallery->type = 'products';
                $gallery->reference = $product->id;
                $gallery->save();
            }
            for($i=0; $i<$request['fileslength']; $i++){
                $image = new Image();
                $file = $request->file('file'.$i);
                $name =  time()."_".$file->getClientOriginalName();
                Storage::disk('public')->put('/images/galleries/products/' . $name,  \File::get($file));
                $image->name = '/storage/images/galleries/products/' . $name;
                $image->slug = Str::slug($name);
                $image->gallery_id = $gallery->id;
                $image->save();
            }
        }


        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        return $product;
    }
}
