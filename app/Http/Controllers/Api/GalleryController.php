<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str as Str;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = Gallery::get();

        return $galleries;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        config(['app.locale' => $request['language']]);
        $gallery = new Gallery();
        $gallery->name = $request['name'];
        $gallery->slug = Str::slug($request['name']);
        $gallery->type = 'default';
        $gallery->save();

        if ($request['fileslength'] != '0' ){
            for($i=0; $i<$request['fileslength']; $i++){
                $image = new Image();
                $file = $request->file('file'.$i);
                $name =  time()."_".$file->getClientOriginalName();
                Storage::disk('public')->put('/images/galleries/default/' . $name,  \File::get($file));
                $image->name = '/storage/images/galleries/default/' . $name;
                $image->slug = Str::slug($name);
                $image->gallery_id = $gallery->id;
                $image->save();
            }
        }


        return $gallery;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gallery = Gallery::findOrFail($id);
        $images = Image::where('gallery_id', $gallery->id)->get();
        $result = [
            'gallery' => $gallery,
            'images' => $images
        ];
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = Gallery::findOrFail($id);
        if($gallery->active){
            $gallery->active = false;
        } else {
            $gallery->active = true;
        }
        $gallery->save();

        return $gallery;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        config(['app.locale' => $request['language']]);
        $gallery = Gallery::findOrFail($id);
        $gallery->name = $request['name'];
        $gallery->slug = Str::slug($request['name']);
        $gallery->type = 'default';
        $gallery->save();

        if ($request['fileslength'] != '0' ){
            for($i=0; $i<$request['fileslength']; $i++){
                $image = new Image();
                $file = $request->file('file'.$i);
                $name =  time()."_".$file->getClientOriginalName();
                Storage::disk('public')->put('/images/galleries/default/' . $name,  \File::get($file));
                $image->name = '/storage/images/galleries/default/' . $name;
                $image->slug = Str::slug($name);
                $image->gallery_id = $gallery->id;
                $image->save();
            }
        }


        return $gallery;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = Gallery::findOrFail($id);
        $gallery->delete();

        return $gallery;
    }
}
