<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Image extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "images";

    protected $fillable = [
        'name',
        'gallery_id',
        'active'
    ];
}
