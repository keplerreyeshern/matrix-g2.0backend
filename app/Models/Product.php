<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Product extends Model
{
    use HasFactory, SoftDeletes, HasTranslations;

    protected $table = "products";

    protected $translatable = ['name', 'slug', 'details', 'description'];

    protected $fillable = [
        'name',
        'details',
        'description',
        'model',
        'key'
    ];

    public function categories(){
        return $this->belongsToMany('App\Models\Category')->withTimestamps();
    }
}
