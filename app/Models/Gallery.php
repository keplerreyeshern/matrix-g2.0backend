<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Gallery extends Model
{
    use HasFactory, SoftDeletes, HasTranslations;

    protected $table = "galleries";

    protected $translatable = ['name', 'slug'];

    protected $fillable = [
        'name',
        'type',
        'reference',
        'active'
    ];
}
